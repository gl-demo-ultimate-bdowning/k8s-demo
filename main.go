package main

import (
	"net/http"

	"github.com/a-h/templ"
	"gitlab.com/gl-demo-ultimate-bdowning/k8s-demo/views"
)

// new config
func main() {
	mux := http.NewServeMux()
	//Comment
	mux.Handle("/", templ.Handler(views.Index()))
	mux.HandleFunc("GET /items/{id}", itemHandler)
	mux.HandleFunc("GET /files/{path...}", filesHandler)

	http.ListenAndServe(":8081", mux)
}

func itemHandler(w http.ResponseWriter, r *http.Request) {
	id := r.PathValue("id") // Dynamically access the path variable
	views.Item(id).Render(r.Context(), w)
}

func filesHandler(w http.ResponseWriter, r *http.Request) {
	path := r.PathValue("path") // Accessing the wildcard path variable
	views.Path(path).Render(r.Context(), w)
}
